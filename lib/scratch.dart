void main() => performTasks();

void performTasks() async {
  task1();
  String task2Result = await task2();
  task3(task2Result);
}

void task1() {
  String result = "task1 - data";
  print("task_1 complete");
}

Future<String> task2() async {
  const threeSecs = Duration(seconds: 3);
  String result = "";
  await Future.delayed(threeSecs, () {
    result = "task2 - data";
    print("task_2 complete");
  });
  return result;
}

void task3(String task2Data) {
  String result = "task3 - data";
  print("task_3 complete with $task2Data");
}
