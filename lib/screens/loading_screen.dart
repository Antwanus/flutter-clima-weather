import 'package:flutter/material.dart';
import 'package:flutter_clima_weather/screens/location_screen.dart';
import 'package:flutter_clima_weather/services/weather.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatefulWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    super.initState();
    getLocationData();
  }

  void getLocationData() async {
    var weatherModel = WeatherModel();
    dynamic data = await weatherModel.getLocationWeather();
    // var location = LocationService();
    // await location.getCurrentLocation();
    // var uri = Uri.https(kOpenWeatherMapHost, kOpenWeatherMapPath, {
    //   'lat': location.latitude!.toStringAsFixed(2),
    //   'lon': location.longitude!.toStringAsFixed(2),
    //   'units': "metric",
    //   'apiKey': kWeatherDataApiKey
    // });
    // var data = await NetworkHelper.getWeatherDataForCurrentLocation(uri);
    // print(data);

    // ROUTE TO LOCATION_SCREEN
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return LocationScreen(
        weatherData: data,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: SpinKitRipple(
          color: Colors.white24,
          size: 250.0,
        ),
      ),
    );
  }
}
