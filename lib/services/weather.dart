import '../util/constants.dart';
import 'location.dart';
import 'networking.dart';

class WeatherModel {
  Future<dynamic> getLocationWeather() async {
    var location = LocationService();
    await location.getCurrentLocation();
    var uri = Uri.https(
      kOpenWeatherMapHost,
      kOpenWeatherMapPath,
      {
        'lat': location.latitude!.toStringAsFixed(2),
        'lon': location.longitude!.toStringAsFixed(2),
        'units': "metric",
        'apiKey': kWeatherDataApiKey
      },
    );
    var data = await NetworkHelper.getWeatherDataForCurrentLocation(uri);

    return data;
  }

  Future<dynamic> getCityWeather({required String city}) async {
    var uri = Uri.https(
      kOpenWeatherMapHost,
      kOpenWeatherMapPath,
      {
        'q': city,
        'units': "metric",
        'apiKey': kWeatherDataApiKey,
      },
    );
    var response = await NetworkHelper.getWeatherByCityName(uri);
    return response;
  }

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
