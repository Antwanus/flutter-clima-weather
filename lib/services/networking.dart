import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

class NetworkHelper {
  static Future<dynamic> getWeatherDataForCurrentLocation(Uri uri) async {
    var response = await http.get(uri);
    if (response.statusCode == 200) {
      return convert.jsonDecode(response.body);
    } else {
      return convert.jsonDecode('{"FAILED": "Response Code != 200"}');
    }
  }

  static Future<dynamic> getWeatherByCityName(Uri uri) async {
    var response = await http.get(uri);
    if (response.statusCode == 200) {
      return convert.jsonDecode(response.body);
    } else {
      return convert.jsonDecode('{"FAILED": "Response Code != 200"}');
    }
  }
}
