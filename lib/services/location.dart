import 'package:geolocator/geolocator.dart';

class LocationService {
  double? latitude;
  double? longitude;

  Future<void> getCurrentLocation() async {
    await Geolocator.checkPermission()
        .then((value) => print('Geolocator.checkPermission ==> $value'));
    await Geolocator.requestPermission()
        .then((value) => print('Geolocator.requestPermission ==> $value'));
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.low);
      print(position);
      latitude = position.latitude;
      longitude = position.longitude;
    } catch (e) {
      print(e);
    }
  }
}
